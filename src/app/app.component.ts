import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { IFilterLHS, IFilterOperator, IAccountValue, ICountry } from './interfaces/interfaces';
import { countries } from './countries';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', '../assets/css/dropdown.scss', '../assets/css/multi-select.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
  accountValues: Array<IAccountValue>;
  countries = countries;
  countriesDisplayed = this.countries.slice();
  filtersFormGroup: FormGroup;
  filters: FormArray;
  lhsValues: Array<IFilterLHS>;
  operatorsValues: Array<Array<IFilterOperator>> = [];
  selectedCountries: Array<Array<ICountry>> = [];
  showCountryDropdown = [];
  constructor(private fb: FormBuilder, private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.initFormGroup();
    this.createFilterLHS();
    this.makeFakeValueForAccount();
  }
  initFormGroup(): void {
    this.filtersFormGroup = this.fb.group({
      filters: this.fb.array([this.createFilter()])
    });
  }
  createFilter(): FormGroup {
    return this.fb.group({
      lhs: ['', Validators.required],
      operator: ['', Validators.required],
      value: ['', Validators.required]
    });
  }
  addFilter(): void {
    this.filters = this.filtersFormGroup.get('filters') as FormArray;
    this.filters.push(this.createFilter());
    this.cdRef.detectChanges();
  }
  onApplyClick() {
    const obj = this.filtersFormGroup.value;
    const arr = [];
    obj.filters.map((el, index) => {
      // tslint:disable-next-line: triple-equals
      if (el.lhs != '' && el.operator != '') {
        if (el.lhs === 'country') {
          if (this.selectedCountries[index]) {
            const cArray = [];
            this.selectedCountries[index].map(country => cArray.push(country.dial_code));
            if (cArray.length) {
              el.value = cArray;
              arr.push(el);
            }
          }
        } else {
          if (this.filtersFormGroup.controls.filters['controls'][index].valid) {
            arr.push(el);
          }
        }
      }
    });
    if (arr.length) {
      console.log({
        filters: arr
      });
    }
  }
  deleteFilter(index): void {
    this.filters = this.filtersFormGroup.get('filters') as FormArray;
    this.filters.removeAt(index);
    this.cdRef.detectChanges();
  }
  onLHSValueChange(index): void {
    this.filters = this.filtersFormGroup.get('filters') as FormArray;
    const ctrl = this.filters.at(index);
    this.operatorsValues[index] = this.createFilterOperator(ctrl.value.lhs);
    switch (ctrl.value.lhs) {
      case 'account':
        break;
      case 'country':
        ctrl['controls'].value.setValidators([]);
        ctrl['controls'].value.updateValueAndValidity();
        break;
      case 'campaign_name':

        break;
      case 'revenue':
        break;
    }
    ctrl['controls'].value.patchValue('');
    ctrl['controls'].operator.patchValue('');
  }
  createFilterLHS(): void {
    this.lhsValues = [
      {
        label: 'Account',
        value: 'account'
      },
      {
        label: 'Country',
        value: 'country'
      },
      {
        label: 'Campaign Name',
        value: 'campaign_name'
      },
      {
        label: 'Revenue',
        value: 'revenue'
      },
    ];
  }
  createFilterOperator(type): Array<IFilterOperator> {
    let arr = [];
    switch (type) {
      case 'account':
        arr = [
          {
            label: 'Contains',
            value: 'contains'
          },
          {
            label: 'Not contains',
            value: 'not_contains'
          }
        ];
        break;
      case 'country':
        arr = [
          {
            label: 'Contains',
            value: 'contains'
          },
          {
            label: 'Not contains',
            value: 'not_contains'
          }
        ];
        break;
      case 'campaign_name':
        arr = [
          {
            label: 'Contains',
            value: 'contains'
          },
          {
            label: 'Not contains',
            value: 'not_contains'
          },
          {
            label: 'Starts with',
            value: 'starts_with'
          }
        ];
        break;
      case 'revenue':
        arr = [
          {
            label: '>',
            value: '>'
          },
          {
            label: '<',
            value: '<'
          },
          {
            label: '>=',
            value: '>='
          },
          {
            label: '<=',
            value: '<='
          },
          {
            label: '=',
            value: '='
          },
          {
            label: '!=',
            value: '!='
          }
        ];
        break;
      default:
        break;
    }
    return arr;
  }
  onCountrySearch(str): void {
    if (str.trim() == '') {
      this.countriesDisplayed = this.countries.slice();
    } else {
      this.countriesDisplayed = this.countries.filter(el => el.name.toLowerCase().includes(str.toLowerCase()));
    }
    this.cdRef.detectChanges();
  }
  onCountryClickOutside(event, index): void {
    if (event && event.value == true) {
      this.showCountryDropdown[index] = false;
    }
  }
  toggleCountry(item, index): void {
    if (!this.selectedCountries[index]) {
      this.selectedCountries[index] = [];
    }
    const arr = this.selectedCountries[index];
    for (let i = 0; i < arr.length; i++) {
      const el = arr[i];
      if (el.code === item.code) {
        arr.splice(i, 1);
        return;
      }
    }
    this.selectedCountries[index].push(item);
    this.cdRef.detectChanges();
  }
  isCountrySelected(country, index): boolean {
    const arr = this.selectedCountries[index];
    if (arr) {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < arr.length; i++) {
        const el = arr[i];
        if (el.code === country.code) {
          return true;
        }
      }
    }
    return false;
  }
  makeFakeValueForAccount() {
    const arr = [];
    Array.from(Array(1000).keys()).map(el => arr.push({ value: el, label: el.toString() }));
    this.accountValues = arr;
  }
  fireAllErrors(formGroup: FormGroup): void {
    const keys = Object.keys(formGroup.controls);
    keys.forEach((field: any) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
        control.markAsDirty({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.fireAllErrors(control);
      } else if (control instanceof FormArray) {
        // tslint:disable-next-line: no-angle-bracket-type-assertion
        (<FormArray>control).controls.forEach((element: FormGroup) => {
          this.fireAllErrors(element);
        });
      }
    });
  }
}
