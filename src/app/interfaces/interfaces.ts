export interface IFilterLHS {
  label: string;
  value: string;
}

export interface IFilterOperator {
  label: string;
  value: string;
}

export interface IAccountValue {
  label: string;
  value: string;
}
export interface ICountry {
  name: string;
  dial_code: string;
  code: string;
}
