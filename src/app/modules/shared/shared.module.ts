import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {DropdownModule} from 'primeng/dropdown';
import {MultiSelectModule} from 'primeng/multiselect';
import { ClickOutside } from 'src/app/directives/click-outside.directive';
import { TwoDigitDecimaNumberDirective } from 'src/app/directives/two-decimal.directive';


@NgModule({
  imports: [
    CommonModule,
    DropdownModule,
    MultiSelectModule
  ],
  declarations: [
    ClickOutside,
    TwoDigitDecimaNumberDirective,
  ],
  exports: [
    DropdownModule,
    MultiSelectModule,
    ClickOutside,
    TwoDigitDecimaNumberDirective
  ]
})
export class SharedModule { }
